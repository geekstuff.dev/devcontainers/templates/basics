#!make

IMAGE ?= devcontainer-geekstuff-basics

all: build

build-all: build-alpine
build-all: build-debian
build-all: build-ubuntu

build-alpine:
	SOURCE=alpine:3.20 $(MAKE) build
	SOURCE=alpine:3.17 $(MAKE) build

build-debian:
	SOURCE=debian:bullseye $(MAKE) build
	SOURCE=debian:bookworm $(MAKE) build

build-ubuntu:
	SOURCE=ubuntu:20.04 $(MAKE) build
	SOURCE=ubuntu:22.04 $(MAKE) build
	SOURCE=ubuntu:24.04 $(MAKE) build

build: .ensure-jsonc-cli
build: .build

.build: TMP_CONFIG ?= /tmp/.${SOURCE}.json
.build:
	test -n "${SOURCE}"
	jsonc-cli -p -i .devcontainer/devcontainer.json \
        | jq -c -r '.image = "'${SOURCE}'"' > ${TMP_CONFIG}
	devcontainer build \
		--workspace-folder . \
		--config ${TMP_CONFIG} \
		--image-name ${IMAGE}-${SOURCE}

.ensure-jsonc-cli:
	@which jsonc-cli 1>/dev/null 2>/dev/null || ( \
		echo ">> Install jsonc-cli" \
		&& wget -O jsonc-cli.tar.gz https://github.com/muhammadmuzzammil1998/jsonc-cli/releases/download/v1.0/jsonc-cli-linux-amd64.tar.gz \
    	&& gunzip jsonc-cli.tar.gz \
    	&& tar xf jsonc-cli.tar \
    	&& mv jsonc-cli-linux-amd64/jsonc-cli ${HOME}/bin \
    	&& rm -rf jsonc-cli-linux-amd64 jsonc-cli.tar \
	)
