# Devcontainers Templates - Basics

This project is a [Devcontainer](.devcontainer/) template pre-built against `debian`
and `alpine`, with `basics`, `direnv` and `starship` devcontainer features.

Note: Not entirely sure about including direnv but it's so tiny even if not used.

## How it's used

The .devcontainer/ configuration in this project can be used with simple entries
in your own `.devcontainer/devcontainer.json` file where you can further customize
it for example:

```json
{
    "name": "my devcontainer",
    "image": "registry.gitlab.com/geekstuff.dev/devcontainers/templates/basics/alpine/3.17",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/docker:0.0.3": {}
    },
    "mounts": [
        {
            "source": "dev-${devcontainerId}-vscode-ext",
            "target": "/home/dev/.vscode-server/extensions",
            "type": "volume"
        },
        {
            "source": "dev-${devcontainerId}-vscode-ext-insiders",
            "target": "/home/dev/.vscode-server-insider/extensions",
            "type": "volume"
        }
    ]
}
```

From the example above:

- In this example, image `alpine/3.17` can be changed to `debian/bullseye`.
- Image can also have a tag, `:latest` for the main branch, or `:vX.Y` from tags in this project.
    - See [here for the docker image list](https://gitlab.com/geekstuff.dev/devcontainers/templates/basics/container_registry)
- That example also wanted docker client inside the devcontainer.
- It also caches VSCode extensions although they seem to often get corrupted so maybe remove
  that if having issues.
